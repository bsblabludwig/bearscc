\name{example/brain_control_example.tsv}
\alias{example/brain_control_example.tsv}
\docType{data}
\title{
%%   ~~ data name/kind ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of the dataset. ~~
}
\usage{data("example/brain_control_example.tsv")}
\format{
  The format is:
 chr "example/brain_control_example.tsv"
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
data(example/brain_control_example.tsv)
## maybe str(example/brain_control_example.tsv) ; plot(example/brain_control_example.tsv) ...
}
\keyword{datasets}
